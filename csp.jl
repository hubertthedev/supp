### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 409c154f-a414-4ac5-b5e7-1efcbde05fcf
begin
	using Pkg
	using Plots
end

# ╔═╡ 84f7c82e-758b-44f0-bf8a-f27b5e3ba4b2
Pkg.activate()

# ╔═╡ 51030286-de61-11eb-2f06-b5f50e3017a3
md" # Assignment Sup"

# ╔═╡ a4def99a-7775-485e-ba19-053c5d4296a1
md" ## Problem 2"

# ╔═╡ 9f8c7668-2864-4f3c-8c15-29736fb6336d
md"> Mark: 25
> Consider the constraint network depicted in Figure 1. 
- The domain of each variable Xı
- is Dı = {1, 2, 3, 4}. 
- The arcs in the network represent the binary constraints. 
- There is one additional unary constraint, 
- X3 = 1.
> Write a
> Julia program that solves the constraint satisfaction problem (CSP) using
> the backtracking method with forward checking and propagation explicitly
> highlighted in the execution."

# ╔═╡ 25f68859-11e9-404a-9b8e-923f2787ebe3
md" ## packages"

# ╔═╡ a3844c92-e996-4d0a-addd-795a7fc5f0d3
md" ## Domain"

# ╔═╡ 6c116158-bf33-4260-8030-eca953026a44
domain = [1,2,3,4]

# ╔═╡ b2463296-9a87-4fce-ab85-c6be6e1ec34c
md" ## Structs"

# ╔═╡ c2f04fe3-bc88-4af3-ac02-03a2ee467979
mutable struct CSP
name#String
value#Vector{domain}
forbidden_values#Vector{domain}
domainRestriction#Int64
end

# ╔═╡ fe1e8cd8-49a5-45e9-9f66-5afa058108c8
struct constraints
vars::Vector{CSP}
constraints::Vector{Tuple{CSP,CSP}}
end

# ╔═╡ 3d073097-a52c-435c-87d7-21cc7ad88055
assignment = rand(setdiff(Set([1,2,3,4]), Set([1,4])))

# ╔═╡ ebc62be7-3ea8-4679-94f1-d11830bf5074
md"## solve function"

# ╔═╡ 3bf548a4-4e76-4699-bee9-0eb6a233c046
function cspSolution(c::constraints, assignment)

	for cur_var in c.vars

		if cur_var.domainRestriction == 4

			return []

		else

			next_val = rand(setdiff(Set([1,2,3,4]),

					Set(cur_var.forbidden_values)))

			cur_var.value = next_val

			for cur_constraint in c.constraints

				if !((cur_constraint[1] == cur_var) || (cur_constraint[2] ==cur_var))

					continue

				else

	if cur_constraint[1] == cur_var push!(cur_constraint[2].forbidden_values,next_val)

						cur_constraint[2].domainRestriction += 1
						
						if cur_constraint[2].domainRestriction == 4
							
						else
						
						end

					else

						push!(cur_constraint[1].forbidden_values, next_val)

						cur_constraint[1].domainRestriction += 1
						
						if cur_constraint[1].domainRestriction ==4
							
						else
							
						end
						
					end
				end
			end
			push!(assignment ,cur_var.name => next_val)
		end
	end
	return assignment
end

# ╔═╡ f11b07c6-3641-4a8e-9bb9-70520d261785
function forward_checking()

end

# ╔═╡ d5b87e9e-9c77-4b52-95b1-bcd33baf1eeb
function backtrack()
    
end

# ╔═╡ 7b95800a-1998-4d07-ab83-7770da0559b2
md"## CSP Declaration"

# ╔═╡ edf12428-6948-4221-b2ea-7b4d8d9be389
x1 = CSP("X1",nothing, [], 0)

# ╔═╡ 1a8b262f-883e-4aa1-86b8-585f759ec0e8
x2 = CSP("X2",nothing, [], 0)

# ╔═╡ 8c91497b-eef2-49f4-9a6a-09437e571655
x3 = CSP("X3",1, [2,3,4], 0)

# ╔═╡ ee50cf97-3e03-4eb2-9d5f-bb508d94506b
x4 = CSP("X4",nothing, [], 0)

# ╔═╡ 3aadd520-af6f-4a81-b157-43334b254ade
x5 = CSP("X5",nothing, [], 0)

# ╔═╡ 165d20b1-927e-4369-9490-05ada0754550
x6 = CSP("X6",nothing, [], 0)

# ╔═╡ 5340a87b-85e9-4bae-b8fb-8a3003dc21ef
x7 = CSP("X7",nothing, [], 0)

# ╔═╡ 9b1c80ac-256d-4ee0-832a-c7daea46a528
md" ### Solution Implementation"

# ╔═╡ 5f707404-9a74-49e0-b9a8-a755eaf6b216
x = constraints([x1, x2, x3, x4, x5, x6, x7], [(x1,x2), (x1,x3), (x1,x4),
(x1,x5), (x1,x6), (x2,x5), (x3,x4), (x4,x5), (x4,x6), (x5,x6), (x6,x7)])

# ╔═╡ 87d89172-660b-4e4a-9d7a-72fd153e6e52
x.constraints

# ╔═╡ d4170341-ef7f-4de8-bbe6-ba00a397ddc9
cspSolution(x,[])

# ╔═╡ Cell order:
# ╟─51030286-de61-11eb-2f06-b5f50e3017a3
# ╟─a4def99a-7775-485e-ba19-053c5d4296a1
# ╟─9f8c7668-2864-4f3c-8c15-29736fb6336d
# ╟─25f68859-11e9-404a-9b8e-923f2787ebe3
# ╠═409c154f-a414-4ac5-b5e7-1efcbde05fcf
# ╠═84f7c82e-758b-44f0-bf8a-f27b5e3ba4b2
# ╟─a3844c92-e996-4d0a-addd-795a7fc5f0d3
# ╠═6c116158-bf33-4260-8030-eca953026a44
# ╟─b2463296-9a87-4fce-ab85-c6be6e1ec34c
# ╠═c2f04fe3-bc88-4af3-ac02-03a2ee467979
# ╠═fe1e8cd8-49a5-45e9-9f66-5afa058108c8
# ╠═3d073097-a52c-435c-87d7-21cc7ad88055
# ╟─ebc62be7-3ea8-4679-94f1-d11830bf5074
# ╠═3bf548a4-4e76-4699-bee9-0eb6a233c046
# ╠═f11b07c6-3641-4a8e-9bb9-70520d261785
# ╠═d5b87e9e-9c77-4b52-95b1-bcd33baf1eeb
# ╟─7b95800a-1998-4d07-ab83-7770da0559b2
# ╠═edf12428-6948-4221-b2ea-7b4d8d9be389
# ╠═1a8b262f-883e-4aa1-86b8-585f759ec0e8
# ╠═8c91497b-eef2-49f4-9a6a-09437e571655
# ╠═ee50cf97-3e03-4eb2-9d5f-bb508d94506b
# ╠═3aadd520-af6f-4a81-b157-43334b254ade
# ╠═165d20b1-927e-4369-9490-05ada0754550
# ╠═5340a87b-85e9-4bae-b8fb-8a3003dc21ef
# ╟─9b1c80ac-256d-4ee0-832a-c7daea46a528
# ╠═5f707404-9a74-49e0-b9a8-a755eaf6b216
# ╠═87d89172-660b-4e4a-9d7a-72fd153e6e52
# ╠═d4170341-ef7f-4de8-bbe6-ba00a397ddc9
