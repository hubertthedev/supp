### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 6287671a-3b85-45a2-a569-e490304e24d6
begin
	using Pkg
	using PlutoUI
	using DataFrames
end

# ╔═╡ 7eaac51e-e315-402c-afca-02ec93037a1c
Pkg.activate()

# ╔═╡ a1b816ac-ddcc-11eb-1921-8971d4236a4c
md" ## Assignment Sup"

# ╔═╡ a7636a91-3284-46b6-8444-a6ac51b793ed
md"### Problem 1."

# ╔═╡ 71163b44-12fa-4e1b-a945-805d47cc0d0b
md" 
> Mark: 25 
> Write a program in Julia that implements the genetic algorithm to find the
> best solution given an initial population (provided by the user at runtime).
> Note that a genetic algorithm includes the following components:"


# ╔═╡ 8bc99cde-b36b-40d8-b350-1688ef9e8bec
md"
- a fitness function;
- a selection probability;
- a crossover point; and
- a mutation probability."

# ╔═╡ 2b2f921f-09bc-4876-bbab-e5f57d3d9e20
md" ### Import Packages"

# ╔═╡ 38f99133-fd70-4c9d-b2de-894d5f4796d2
md" ## Params"

# ╔═╡ a2890516-120f-48d4-af1b-139e4d0ee5f8
uniformRate = 0.5

# ╔═╡ 09a387b6-8f6d-4407-a10e-3933063c9be6
mutationRate = 0.015

# ╔═╡ 63165818-88c1-4cdb-8228-022c3b42054d
byte = 0:1

# ╔═╡ bb46a7bb-ace0-438e-9fce-a0f172cdb8de
md"## Structs"

# ╔═╡ 5efe9f33-ca3a-4505-91e9-1b3fa170761d
mutable struct entity
	genes
	fitness
end

# ╔═╡ a0d1e63d-1ae3-4df8-a7e1-c616d184c6f6
mutable struct population
	entities
end

# ╔═╡ 8cc04bea-751d-4ee4-a733-86961fdc7fc0
md" ### Generate a population"

# ╔═╡ 982f0a98-0e50-4de6-a6d6-dc7896eb18dd
function generate(pop,gLength)
	Population =Array{entity}(undef, pop)
	for i = 1:pop
		Population[i] = entity(rand(byte,gLength),0)
	end
	p = population(Population)
	return p
end

# ╔═╡ 4a2f4d62-6005-4070-8828-f07cac092708
p = generate(100,6)

# ╔═╡ d4f29f26-6680-499c-b27a-abe3c9c1de57
md" ##### Access Fitness of entity @ index"

# ╔═╡ 56732db0-6456-4d60-8b16-cab01bd85950
p.entities[1].fitness

# ╔═╡ 1c28627e-2172-4f25-8795-0b7243302014
md"##### Access GenePool of entity @ index"

# ╔═╡ 0f28934b-fe80-444f-8b98-d77e690056f3
p.entities[1].genes

# ╔═╡ 6c2c006e-1ed7-416d-a287-3953cecf2e44
x = length(p.entities)

# ╔═╡ e79ec987-00e1-4954-ac2f-8edf0731b8b5
p.entities[1].genes[1]

# ╔═╡ 4040f22d-7644-4eac-a32e-d1fa4b63896e
md" ## Fittest Function"

# ╔═╡ f5de487a-396d-430f-ac1b-dea71f8661f2
function getFittest(pop)
	pL = size(pop.entities[1].genes)
	fittest = entity(zeros(pL),0)
	l = length(pop.entities)
	for i = 1:l
		
			if fittest.fitness <= pop.entities[i].fitness
				fittest = pop.entities[i]
			end
	
	end
	return fittest
end

# ╔═╡ cc109a4b-02f4-4c8c-9cf4-550fa62f03c9
getFittest(p)

# ╔═╡ 20734d9f-7915-4f04-b76c-befe00bf4156
md" #### Fitness Function"

# ╔═╡ 98db0f16-3491-4a2d-b490-99e08d812a4c
function getFitness(ent,solution)
	l = length(ent.genes)
	ent.fitness = 0
	
	for i = 1:l
		
		if ent.genes[i] == solution[i]
			ent.fitness +=1

		end
		
	end
	return ent.fitness
end

# ╔═╡ c010220a-84e8-4d23-9988-1fbbdab391e1
function maxFit(solution)
	max = length(solution)
	return max
end

# ╔═╡ 2a57be20-c593-4233-8638-18e7a2516d20
md" #### Selection"

# ╔═╡ 7ecf3d75-ce78-49d9-bd16-614ad915141e
function getPopSize(pop)
	p = length(pop.entities)
	
	return p
end

# ╔═╡ b131cf41-2bf9-4739-b2d7-6921fe5b674f
function select(pop)
	
	l = length(pop.entities)
	gl = length(pop.entities[1].genes)
	
	total = getPopSize(pop)
	
	CR = 0.31*total
	
	c = floor(Int64,CR)

	selected = generate(c,gl)
	selection =[p]
	for i = 1:c
		rIndex = rand(1:l)
		selection = pop.entities[rIndex]
		if selection.fitness > 0
		selected.entities[i] = selection
		end
	end
	fittest = getFittest(selected)
	return fittest
end

# ╔═╡ a3d059a1-6bb3-4d10-adcb-8b16c497d7df
p.entities[1].fitness

# ╔═╡ 249e62e6-a2f3-4d79-8b0f-3fcaedf18320
md" #### Crossover Point"

# ╔═╡ 10c329d7-8e6b-4d20-9612-72b8af297933
function crossover(ent1,ent2)
	
	l=length(ent1.genes)
	gl = length(ent1.genes)
	newEnt = entity(rand(Int,gl),0)
	hg = gl/2
	half = floor(Int64,hg)
	h = half + 1
	for i = 1:l
			p1Ca = ent1.genes[1:half]
			p1Cb = ent1.genes[h:end]
			p2Ca = ent2.genes[1:half]
			p2Cb = ent2.genes[h:end]
		
		yChromosome = [p1Ca ; p2Cb]
		xChromosome = [p2Ca ; p1Cb]
			
			offspring1 = [p1Ca;p2Cb]
			offspring2 = [p2Ca ; p1Cb]
		
			if rand() <= uniformRate
			newEnt = entity(offspring1,ent1.fitness)
		else
			newEnt = entity(offspring2,ent2.fitness)
		end
		
	
	end
	return newEnt
end

# ╔═╡ bc964db8-240b-4b27-be26-53a0a71d8954
ent1 = select(p)

# ╔═╡ 18bc0354-d3c1-4e2d-856b-67ef039ff456
ent2 = select(p)

# ╔═╡ 1bde21b7-e5a0-4b78-bd00-71be91f5778a
new = crossover(ent1,ent2)

# ╔═╡ 6dccd9ed-7b86-4bd6-aa13-d1923bc5684b
begin
	l=length(ent1.genes)
		gl = length(ent1.genes)
		newEnt = entity(rand(Int,gl),0)
		hg = gl/2
		half = floor(Int64,hg)
		h = half +=1
		for i = 1:l
				p1Ca = ent1.genes[1:half]
				p1Cb = ent1.genes[h:end]
				p2Ca = ent2.genes[1:half]
				p2Cb = ent2.genes[h:end]
			
			yChromosome = [p1Ca ; p2Cb]
			xChromosome = [p2Ca ; p1Cb]
				
				offspring1 = rand(yChromosome,l)
				offspring2 = rand(xChromosome,l)
		
		end

	
end

# ╔═╡ 8ca36cf2-970a-4b63-9f16-f3111c89fa15
md" #### Mutation"

# ╔═╡ 5d5d857e-c19b-4c82-819a-3ed830513d4b
function mutate(ent)
	
	l=length(ent.genes)
	for i = 1:l
		if rand() <= mutationRate
			ent.genes[i] = rand(byte)
		end
	end

end

# ╔═╡ 4cf9e4be-10ee-4467-b5d2-016640ea1bed
md" ## Evolve Population"

# ╔═╡ c1b9fbbb-ae48-4d7e-9132-49b9bf965787
function evolve(pop)
	l = length(pop.entities)
	newPop = population(pop.entities)
	

	
	for i in 1:l
		ent1 = select(pop)
        ent2 = select(pop)
        newIndiv = crossover(ent1, ent2)
        newPop.entities[i] = newIndiv

	end
	
	total = length(newPop.entities)
	
	MR = 0.69*total
	
	m = floor(Int64,MR)
	for i = 1:m

	mutate(newPop.entities[i])
	end
	
    return newPop
    
end

# ╔═╡ 11d02f4a-8774-4262-b5fc-5e54431bbc87
md" ## Generate Solution"

# ╔═╡ 20b0f4f8-93ec-4336-b55f-9bd8b761adf0
function solution(input)
	s = rand(byte,input)
	
	return s
end

# ╔═╡ 7a075a10-036e-45fb-9fd2-5fffbc050442
md" # Test"

# ╔═╡ fe1d5acb-68fa-4305-9882-4c69985ff0dc
function solve(pop,solution)
	generationCount = 0;
	
while(getFitness(getFittest(pop),solution) < maxFit(solution))
  generationCount += 1
		i = 1
		fitness[i] = getFittest(pop).fitness
		gen[i] = generationCount
		i+=1
  println("\n Generation: ", generationCount ," Fittest: ", getFittest(pop).fitness)
		

  myPop = evolve(pop)
	end
	println("\nBest Solution found!")
	println("Generation ",generationCount)
	println("\nGenes:")
	println(getFittest(pop).genes)
	

end

# ╔═╡ c8867f39-bfcf-41c7-ae09-add289ba56e6
md" ### generate population & solution"

# ╔═╡ f69ea6cd-8efb-40e7-8a34-80faafe0ae94
p1 = generate(50,6)

# ╔═╡ 08098f0a-9ff0-4c0b-9cd1-2ed70e87f833
getFittest(p1)

# ╔═╡ f35fc320-e9c5-480f-9f85-6f0d72fb292d
getFittest(p1)

# ╔═╡ f0ade14f-5420-400e-8641-43d09c9a697b
getFittest(p1).fitness

# ╔═╡ e6ddb8d5-1c89-4c38-9b24-c07c338267fd
s = solution(6)

# ╔═╡ c060a10e-208c-4453-8448-6c8943af50b9
getFitness(getFittest(p1),s)

# ╔═╡ 45a2cbfa-636a-4b77-882b-f84dc0d7b4b6
maxFit(s)

# ╔═╡ 31d40457-646c-4407-aebf-26b2e1f2747d
getFitness(p1.entities[1],s)

# ╔═╡ c5fd081f-ecbf-4ce3-a9f0-e14f1402f47e
with_terminal() do
	solve(p1,s)
end

# ╔═╡ 7730c43e-b753-4573-8ee7-f2bf5e2b8211
getFittest(p1).fitness

# ╔═╡ 9cbbda82-4b1f-40fd-9240-4a59e7b6a8ec
getFitness(getFittest(p1),s) == getFittest(p1).fitness

# ╔═╡ Cell order:
# ╟─a1b816ac-ddcc-11eb-1921-8971d4236a4c
# ╟─a7636a91-3284-46b6-8444-a6ac51b793ed
# ╟─71163b44-12fa-4e1b-a945-805d47cc0d0b
# ╟─8bc99cde-b36b-40d8-b350-1688ef9e8bec
# ╟─2b2f921f-09bc-4876-bbab-e5f57d3d9e20
# ╠═6287671a-3b85-45a2-a569-e490304e24d6
# ╠═7eaac51e-e315-402c-afca-02ec93037a1c
# ╟─38f99133-fd70-4c9d-b2de-894d5f4796d2
# ╠═a2890516-120f-48d4-af1b-139e4d0ee5f8
# ╠═09a387b6-8f6d-4407-a10e-3933063c9be6
# ╠═63165818-88c1-4cdb-8228-022c3b42054d
# ╟─bb46a7bb-ace0-438e-9fce-a0f172cdb8de
# ╠═5efe9f33-ca3a-4505-91e9-1b3fa170761d
# ╠═a0d1e63d-1ae3-4df8-a7e1-c616d184c6f6
# ╟─8cc04bea-751d-4ee4-a733-86961fdc7fc0
# ╠═982f0a98-0e50-4de6-a6d6-dc7896eb18dd
# ╠═4a2f4d62-6005-4070-8828-f07cac092708
# ╟─d4f29f26-6680-499c-b27a-abe3c9c1de57
# ╠═56732db0-6456-4d60-8b16-cab01bd85950
# ╟─1c28627e-2172-4f25-8795-0b7243302014
# ╠═0f28934b-fe80-444f-8b98-d77e690056f3
# ╠═6c2c006e-1ed7-416d-a287-3953cecf2e44
# ╠═e79ec987-00e1-4954-ac2f-8edf0731b8b5
# ╟─4040f22d-7644-4eac-a32e-d1fa4b63896e
# ╠═f5de487a-396d-430f-ac1b-dea71f8661f2
# ╠═cc109a4b-02f4-4c8c-9cf4-550fa62f03c9
# ╟─20734d9f-7915-4f04-b76c-befe00bf4156
# ╠═98db0f16-3491-4a2d-b490-99e08d812a4c
# ╠═c010220a-84e8-4d23-9988-1fbbdab391e1
# ╟─2a57be20-c593-4233-8638-18e7a2516d20
# ╠═7ecf3d75-ce78-49d9-bd16-614ad915141e
# ╠═b131cf41-2bf9-4739-b2d7-6921fe5b674f
# ╠═a3d059a1-6bb3-4d10-adcb-8b16c497d7df
# ╟─249e62e6-a2f3-4d79-8b0f-3fcaedf18320
# ╠═10c329d7-8e6b-4d20-9612-72b8af297933
# ╠═bc964db8-240b-4b27-be26-53a0a71d8954
# ╠═18bc0354-d3c1-4e2d-856b-67ef039ff456
# ╠═1bde21b7-e5a0-4b78-bd00-71be91f5778a
# ╠═6dccd9ed-7b86-4bd6-aa13-d1923bc5684b
# ╟─8ca36cf2-970a-4b63-9f16-f3111c89fa15
# ╠═5d5d857e-c19b-4c82-819a-3ed830513d4b
# ╟─4cf9e4be-10ee-4467-b5d2-016640ea1bed
# ╠═c1b9fbbb-ae48-4d7e-9132-49b9bf965787
# ╟─11d02f4a-8774-4262-b5fc-5e54431bbc87
# ╠═20b0f4f8-93ec-4336-b55f-9bd8b761adf0
# ╟─7a075a10-036e-45fb-9fd2-5fffbc050442
# ╠═fe1d5acb-68fa-4305-9882-4c69985ff0dc
# ╠═08098f0a-9ff0-4c0b-9cd1-2ed70e87f833
# ╠═c060a10e-208c-4453-8448-6c8943af50b9
# ╠═f35fc320-e9c5-480f-9f85-6f0d72fb292d
# ╠═45a2cbfa-636a-4b77-882b-f84dc0d7b4b6
# ╠═f0ade14f-5420-400e-8641-43d09c9a697b
# ╠═31d40457-646c-4407-aebf-26b2e1f2747d
# ╟─c8867f39-bfcf-41c7-ae09-add289ba56e6
# ╠═f69ea6cd-8efb-40e7-8a34-80faafe0ae94
# ╠═e6ddb8d5-1c89-4c38-9b24-c07c338267fd
# ╠═c5fd081f-ecbf-4ce3-a9f0-e14f1402f47e
# ╠═7730c43e-b753-4573-8ee7-f2bf5e2b8211
# ╠═9cbbda82-4b1f-40fd-9240-4a59e7b6a8ec
